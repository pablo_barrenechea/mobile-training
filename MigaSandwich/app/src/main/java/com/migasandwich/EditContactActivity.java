package com.migasandwich;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.migasandwich.model.ContactItem;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;


public class EditContactActivity extends ActionBarActivity {

    private ContactItem contactItem;
    private int contactPosition;

    /**
     * Form fields
     */
    private EditText contactNameEditText;
    private EditText contactEmailEditText;
    private EditText contactNumberEditText;
    private Button saveContactButton;
    private ImageView contactImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        Intent i = getIntent();
        contactItem = (ContactItem) i.getSerializableExtra(ContactListActivity.CONTACT_ITEM);
        if( contactItem != null ){
            contactPosition = i.getIntExtra(ContactListActivity.CONTACT_POSITION, -1);
            contactImageView = (ImageView) findViewById(R.id.contactImageView);
            populateEditForm();
        }else{
            populateAddForm();
        }

    }

    private void populateAddForm(){
        contactNameEditText = (EditText)findViewById(R.id.contactNameEditText);
        contactEmailEditText = (EditText) findViewById(R.id.contactEmailEditText);
        contactNumberEditText = (EditText) findViewById(R.id.contactNumberEditText);
        contactImageView = (ImageView) findViewById(R.id.contactImageView);
        contactItem = new ContactItem();
        saveContactButton = (Button)findViewById(R.id.saveButton);
        saveContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                contactItem.setName(contactNameEditText.getText().toString());
                contactItem.setEmail(contactEmailEditText.getText().toString());
                contactItem.setNumber(contactNumberEditText.getText().toString());
                returnIntent.putExtra(ContactListActivity.ADD_CONTACT_RESULT_FIELD, contactItem);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
        contactImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void populateEditForm() {
        contactNameEditText = (EditText)findViewById(R.id.contactNameEditText);
        contactEmailEditText = (EditText) findViewById(R.id.contactEmailEditText);
        contactNumberEditText = (EditText) findViewById(R.id.contactNumberEditText);
        if (contactItem != null)  {
            if( !contactItem.getName().isEmpty() )
                contactNameEditText.setText(contactItem.getName());
            if( !contactItem.getEmail().isEmpty() )
                contactEmailEditText.setText(contactItem.getEmail());
            if( !contactItem.getNumber().isEmpty() )
                contactNumberEditText.setText(contactItem.getNumber());
            if( !contactItem.getImageUri().isEmpty() )
                contactImageView.setImageURI(Uri.parse(contactItem.getImageUri()));
        }

        saveContactButton = (Button)findViewById(R.id.saveButton);
        saveContactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra(ContactListActivity.CONTACT_POSITION, contactPosition);
                contactItem.setName(contactNameEditText.getText().toString());
                contactItem.setEmail(contactEmailEditText.getText().toString());
                contactItem.setNumber(contactNumberEditText.getText().toString());
                returnIntent.putExtra(ContactListActivity.EDIT_CONTACT_RESULT_FIELD, contactItem);
                setResult(RESULT_OK, returnIntent);
                finish();
            }
        });
        contactImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectImage();
            }
        });
    }

    private void selectImage() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(EditContactActivity.this);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Take Photo")) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(intent, 1);
                } else if (items[item].equals("Choose from Gallery")) {
                    Intent intent = new Intent(
                            Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    intent.setType("image/*");
                    startActivityForResult(
                            Intent.createChooser(intent, "Select File"), 2);
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void onActivityResult(int reqCode, int resCode, Intent data){
        if (resCode == RESULT_OK) {
            if (reqCode == 1) {
                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
                File destination = new File(Environment.getExternalStorageDirectory(),
                        System.currentTimeMillis() + ".jpg");
                FileOutputStream fo;
                try {
                    destination.createNewFile();
                    fo = new FileOutputStream(destination);
                    fo.write(bytes.toByteArray());
                    fo.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                contactItem.setImageUri(destination.toString());
                contactImageView.setImageBitmap(thumbnail);
            } else if (reqCode == 2) {
                contactImageView.setImageURI(data.getData());
                contactItem.setImageUri(data.getDataString());
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_edit_contact, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
