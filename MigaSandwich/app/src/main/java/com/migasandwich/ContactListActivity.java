package com.migasandwich;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewStub;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.Toast;

import com.migasandwich.adapter.ContactListAdapter;
import com.migasandwich.adapter.GridViewAdapter;
import com.migasandwich.model.ContactItem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;


public class ContactListActivity extends ActionBarActivity implements SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    ViewStub listview, gridview, header;
    private ListView contactListView;
    private GridView grid;
    private GridViewAdapter gridViewAdapter;
    private boolean listViewSelected = true;

    private List<ContactItem> contactList;
    private List<ContactItem> originalContactList;
    private ContactListAdapter contactAdapter;
    private ProgressDialog progressDialog;
    private Button addContact;
    private SearchView searchView;
    public static final String CONTACT_ITEM = "contact_item";
    public static final String CONTACT_POSITION = "contact_position";
    public static final int EDIT_CONTACT_CODE = 1;
    public static final String EDIT_CONTACT_RESULT_FIELD = "edit";
    public static final String ADD_CONTACT_RESULT_FIELD = "add";
    public static final int NEW_CONTACT_CODE = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        contactList = new ArrayList<>();
        originalContactList = new ArrayList<>();

        header = (ViewStub) findViewById(R.id.header);
        listview = (ViewStub) findViewById(R.id.list);
        gridview = (ViewStub) findViewById(R.id.grid);

        header.inflate();
        //inflate the layouts
        listview.inflate();
        gridview.inflate();

        contactListView = (ListView)findViewById(R.id.listView);
        contactListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                onLongClickContact(contactList.get(position).getName(), position);
                return false;
            }
        });
        grid = (GridView) findViewById(R.id.grid);
        grid.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                onLongClickContact(contactList.get(position).getName(), position);
                return false;
            }
        });

        //setear adapter dependiendo la vista
        setAdapters();
        loadContacts();
        initSearchView();
        bindButtons();
    }

    protected void bindButtons(){
        addContact = (Button) findViewById(R.id.add_contact_button);
        addContact.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
               /*
                ContactItem gasty = new ContactItem();
                gasty.setName("Gasty1");
                gasty.setEmail("gmanoli@devspark.com1");
                contactList.add(gasty);
                originalContactList.add(gasty);
                notifyViewAdapters();
                */
                Intent intent = new Intent(ContactListActivity.this, EditContactActivity.class);
                startActivityForResult(intent, NEW_CONTACT_CODE);
            }
        });

    }

    private void initSearchView() {
        searchView = (SearchView) findViewById(R.id.searchView);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        showResults(query);
        return false;
    }

    public boolean onQueryTextChange(String newText) {
        showResults(newText);
        return false;
    }

    private void resetSearchResults(){
        contactList = null;
        contactList = new ArrayList<>();
        for( ContactItem item : originalContactList ){
            contactList.add(item);
        }
    }

    private void showResults(String query) {
        if( query.equals("") ){
            resetSearchResults();
            updateResults(contactList);
        }else{
            List<ContactItem> tmpList = new ArrayList<>();
            for( ContactItem item : originalContactList ){
                if( item.getName().toLowerCase().contains(query.toLowerCase()) ){
                    tmpList.add(item);
                }
            }
            contactList = tmpList;
            updateResults(tmpList);
        }
    }

    private void updateResults(List<ContactItem> list) {
        if(listViewSelected) {
            contactAdapter.setContactList(list);
            contactAdapter.notifyDataSetChanged();
        }
        else{
            gridViewAdapter = new GridViewAdapter(this,R.layout.component_grid, list);
            grid.setAdapter(gridViewAdapter);
            gridViewAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public boolean onClose() {
        resetSearchResults();
        contactAdapter.setContactList(contactList);
        contactAdapter.notifyDataSetChanged();
        return false;
    }

    public void onLongClickContact(String name, final int position) {
        //Creo labels de opciones
        final String[] labels = new String[7];
        labels[0] = getString(R.string.menuItem_editContact);
        labels[1] = getString(R.string.menuItem_deleteContact);
        labels[2] =  getString(R.string.menuItem_email);
        labels[3] =  getString(R.string.menuItem_sms);
        labels[4] =  getString(R.string.menuItem_call);
        labels[5] =  getString(R.string.menuItem_share);
        labels[6] = getString(R.string.menuItem_cancel);
        //Evento clic en opcion
        DialogInterface.OnClickListener onItemClick = new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(labels[which].equals(getString(R.string.menuItem_cancel)))
                    dialog.cancel();
                else if(labels[which].equals(getString(R.string.menuItem_editContact))){
                    Intent intent = new Intent(ContactListActivity.this, EditContactActivity.class);
                    intent.putExtra(CONTACT_ITEM, contactList.get(position));
                    intent.putExtra(CONTACT_POSITION, position);
                    startActivityForResult(intent, EDIT_CONTACT_CODE);
                }else if(labels[which].equals(getString(R.string.menuItem_deleteContact))){
                    showConfirmDeleteDialog(position);
                }else if( labels[which].equals(getString(R.string.menuItem_email))) {
                    startEmailIntent(position);
                } else if( labels[which].equals(getString(R.string.menuItem_sms))) {
                    startSmsIntent(position);
                } else if( labels[which].equals(getString(R.string.menuItem_call))) {
                    startCallIntent(position);
                } else if (labels[which].equals(getString(R.string.menuItem_share))) {
                    try {
                        shareContactIntent(position);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(name);
        builder.setItems(labels, onItemClick);

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void startEmailIntent(int position){
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{contactList.get(position).getEmail()});
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "subject");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "message");
        emailIntent.setType("message/rfc822");
        startActivity(Intent.createChooser(emailIntent, "Choose an Email client :"));
    }

    private void startSmsIntent(int position) {
        Intent sendIntent = new Intent(Intent.ACTION_VIEW);
        sendIntent.putExtra("sms_body", "default content");
        sendIntent.putExtra("address", contactList.get(position).getNumber());
        sendIntent.setType("vnd.android-dir/mms-sms");
        startActivity(sendIntent);
    }

    private void startCallIntent(int position) {
        Intent callIntent = new Intent(Intent.ACTION_CALL);
        callIntent.setData(Uri.parse("tel:" + contactList.get(position).getNumber()));
        startActivity(callIntent);
    }

    private void shareContactIntent(int position) throws IOException {
        File vcfFile = new File(this.getExternalFilesDir(null), "generated.vcf");
        FileWriter fw = new FileWriter(vcfFile);
        fw.write("BEGIN:VCARD\r\n");
        fw.write("VERSION:3.0\r\n");
        fw.write("N:" + contactList.get(position).getName() + "\r\n");
        fw.write("FN:" + contactList.get(position).getName() + "\r\n");
        fw.write("TEL;TYPE=WORK,VOICE:" + contactList.get(position).getNumber() + "\r\n");
        fw.write("TEL;TYPE=HOME,VOICE:" + contactList.get(position).getNumber() + "\r\n");
        fw.write("EMAIL;TYPE=PREF,INTERNET:" + contactList.get(position).getEmail() + "\r\n");
        fw.write("END:VCARD\r\n");
        fw.close();

        Intent i = new Intent();
        i.setAction(android.content.Intent.ACTION_VIEW);
        i.setDataAndType(Uri.fromFile(vcfFile), "text/x-vcard");
        startActivity(i);
    }

    private void showConfirmDeleteDialog(final int position){
        AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle(getString(R.string.deleteConfirmation));
        //dialog.setIcon(R.drawable.icon);

        dialog.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                progressDialog = ProgressDialog.show(ContactListActivity.this, "", getString(R.string.updating), true);

                AsyncTask<String, Void, Void> task = new AsyncTask<String, Void, Void>() {

                    @Override
                    protected Void doInBackground(String... args0) {
                        deleteFromRealm(contactList.get(position));
                        for (int i = 0; i < originalContactList.size(); i++) {
                            if (originalContactList.get(i).equals(contactList.get(position))) {
                                originalContactList.remove(i);
                                break;
                            }
                        }
                        contactList.remove(position);
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void result) {
                        if (progressDialog != null) {
                            progressDialog.dismiss();
                            progressDialog = null;
                        }
                        notifyViewAdapters();
                    }

                };

                task.execute();
            }
        });

        dialog.setNegativeButton(R.string.menuItem_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        dialog.show();
    }

    private void deleteFromRealm(ContactItem item) {
        if (item == null) return;
        Realm realm = null;
        try {
            realm = Realm.getInstance(this);
            realm.beginTransaction();

            ContactItem dbItem = realm.where(ContactItem.class).equalTo("id", item.getId()).findFirst();
            if (dbItem != null) {
                dbItem.removeFromRealm();
            }
            realm.commitTransaction();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    private void loadContacts() {
        if (contactList == null)
            return;
        contactList.clear();
        Realm realm = null;
        try {
            realm = Realm.getInstance(this);

            for (ContactItem item : realm.allObjects(ContactItem.class)) {
                //Copy the ToDoItem in order to insert to the local list
                ContactItem itemToInsert = new ContactItem();
                itemToInsert.setId(item.getId());
                itemToInsert.setName(item.getName());
                itemToInsert.setEmail(item.getEmail());
                itemToInsert.setNumber(item.getNumber());
                itemToInsert.setImageUri(item.getImageUri());
                contactList.add(itemToInsert);
                originalContactList.add(itemToInsert);
            }
            contactAdapter.notifyDataSetChanged();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_contact_list, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
       int id = item.getItemId();

        if (id == R.id.action_changeView) {
            changeView();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        saveContactList(originalContactList);
        super.onPause();
    }

    private void saveContactList(List<ContactItem> contactList) {
        if (contactList == null)
            return;
        Realm realm = null;
        try {
            realm = Realm.getInstance(this);
            realm.beginTransaction();
            realm.copyToRealmOrUpdate(contactList);
            realm.commitTransaction();
        } finally {
            if (realm != null) {
                realm.close();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        int position = -1;
        ContactItem result;
        if( resultCode == RESULT_OK){
            if (requestCode == EDIT_CONTACT_CODE) {
                result = (ContactItem)data.getSerializableExtra(EDIT_CONTACT_RESULT_FIELD);
                position = data.getIntExtra(CONTACT_POSITION, -1);
            }else{
                result = (ContactItem)data.getSerializableExtra(ADD_CONTACT_RESULT_FIELD);
            }

            if (result != null && !result.getName().isEmpty()) {
                if (position >= 0) {
                    contactList.set(position, result);
                    originalContactList.set(position, result);
                } else {
                    contactList.add(result);
                    originalContactList.add(result);
                }
                notifyViewAdapters();
            }
        }else if (resultCode == RESULT_CANCELED) {
            Toast.makeText(this, getString(R.string.user_cancel), Toast.LENGTH_SHORT).show();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void notifyViewAdapters() {
        if (contactAdapter != null){
            contactAdapter.notifyDataSetChanged();
        }
        if (gridViewAdapter != null){
            gridViewAdapter.notifyDataSetChanged();
        }
    }

    private void setAdapters() {
        if(listViewSelected) {
            contactAdapter = new ContactListAdapter(this, contactList);
            contactListView.setAdapter(contactAdapter);
        }

        else {
            gridViewAdapter = new GridViewAdapter(this,R.layout.component_grid, contactList);
            grid.setAdapter(gridViewAdapter);
        }

    }

    private void changeView() {

        //if the current view is the listview, passes to gridview
        if(listViewSelected) {
            listview.setVisibility(View.GONE);
            gridview.setVisibility(View.VISIBLE);
            listViewSelected = false;
            setAdapters();
        }

        else {
            gridview.setVisibility(View.GONE);
            listview.setVisibility(View.VISIBLE);
            listViewSelected = true;
            setAdapters();
        }

    }
}
