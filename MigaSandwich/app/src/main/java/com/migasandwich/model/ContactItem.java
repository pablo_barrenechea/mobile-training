package com.migasandwich.model;

import java.io.Serializable;

import android.graphics.Bitmap;
import android.net.Uri;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by pbarrenechea on 7/30/15.
 */
public class ContactItem extends RealmObject implements Serializable {

    @PrimaryKey
    private long id = System.nanoTime();
    private String name = "";
    private String email = "";
    private String number = "";
    private String imageUri = "";

    public long getId() {

        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;

    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }
}
