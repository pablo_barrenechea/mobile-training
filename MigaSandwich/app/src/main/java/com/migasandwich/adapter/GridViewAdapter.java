package com.migasandwich.adapter;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.migasandwich.R;
import com.migasandwich.model.ContactItem;


public class GridViewAdapter extends ArrayAdapter<ContactItem>{
	
	public GridViewAdapter(Context context, int layoutResourceId, List<ContactItem> data) {
		super(context, layoutResourceId, data); 
	}

	@Override 
	public View getView(int position, View convertView, ViewGroup parent) { 
		/*View curView = convertView;
		
		if (curView == null) 
		{ 
			LayoutInflater inflater = (LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			curView = inflater.inflate(R.layout.component_grid, null); 

		}*/
		ContactViewHolder holder;
		if (convertView == null) {
			LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
			//convertView = inflater.inflate(R.layout.contact_item, parent, false);
			convertView = inflater.inflate(R.layout.component_grid, parent, false);

			holder = new ContactViewHolder();
            holder.imageView = (ImageView)convertView.findViewById(R.id.contactImageView);
			holder.nameText = (TextView)convertView.findViewById(R.id.contactNameTextView);

			convertView.setTag(holder);
		} else {
			holder = (ContactViewHolder)convertView.getTag();
		}

		ContactItem item = getItem(position);
		if (item != null) {
            if (!item.getImageUri().isEmpty()) {
                holder.imageView.setImageURI(Uri.parse(item.getImageUri()));
            }
			holder.nameText.setText(item.getName());
		}
		
		return convertView;
	}

	public static class ContactViewHolder {
        public ImageView imageView;
        public TextView nameText;
	}
}


